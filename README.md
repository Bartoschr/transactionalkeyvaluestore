
# TransactionalKeyValueStoreCLI
### Requirements
 - Xcode 12+
### Usage
- open Xcode
- select TransactionalKeyValueStoreTestCLI Terminal scheme
- Product -> Run
- Terminal.app should be launched
### Commands

- SET <key> <value> // store the value for key
- GET <key>         // return the current value for key
- DELETE <key>      // remove the entry for key
- COUNT <value>     // return the number of keys that have the given value
- BEGIN             // start a new transaction
- COMMIT            // complete the current transaction
- ROLLBACK          // revert to state prior to BEGIN call
- QUIT              // terminates CLI

### Test 
- select TransactionalKeyValueStoreTest scheme
- Product -> Test
