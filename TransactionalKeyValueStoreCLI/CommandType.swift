// 

import Foundation

enum CommandType: String {
    case set = "SET"
    case get = "GET"
    case delete = "DELETE"
    case count = "COUNT"
    case begin = "BEGIN"
    case commit = "COMMIT"
    case rollback = "ROLLBACK"
    case quit = "QUIT"
}
