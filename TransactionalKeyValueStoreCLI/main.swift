// 

import Foundation

let consoleIO = ConsoleIO()
consoleIO.printUsage()
let transactionalStore = TransactionalStore()
var shouldQuit = false
while !shouldQuit {
    print(">", terminator: " ")
    let args = consoleIO.getInput()
    guard args.count > 0 else {continue}
    guard let commandType = CommandType(rawValue: args[0]) else {
        consoleIO.writeMessage("unsupported argument")
        continue
    }
    switch commandType {
    case .set:
        guard args.count == 3 else {
            consoleIO.writeMessage("unexpected number of arguments")
            continue
        }
        transactionalStore.set(args[1], args[2])
    case .get:
        guard args.count == 2 else {
            consoleIO.writeMessage("unexpected number of arguments")
            continue
        }
        if let value = transactionalStore.get(args[1]) {
            consoleIO.writeMessage(value)
        } else {
            consoleIO.writeMessage("key not set")
        }
    case .delete:
        guard args.count == 2 else {
            consoleIO.writeMessage("unexpected number of arguments")
            continue
        }
        transactionalStore.delete(args[1])
    case .count:
        guard args.count == 2 else {
            consoleIO.writeMessage("unexpected number of arguments")
            continue
        }
        consoleIO.writeMessage("\(transactionalStore.count(args[1]))")
    case .begin:
        guard args.count == 1 else {
            consoleIO.writeMessage("unexpected number of arguments")
            continue
        }
        transactionalStore.begin()
    case .commit:
        guard args.count == 1 else {
            consoleIO.writeMessage("unexpected number of arguments")
            continue
        }
        do {
            try transactionalStore.commit()
        } catch {
            consoleIO.writeMessage("no transaction")
        }
    case .rollback:
        guard args.count == 1 else {
            consoleIO.writeMessage("unexpected number of arguments")
            continue
        }
        if transactionalStore.rollBack() == nil {
            consoleIO.writeMessage("no transaction")
        }
    case .quit:
        shouldQuit.toggle()
    }
}

