// 

import Foundation

enum OutputType {
    case error
    case standard
}

class ConsoleIO {
    func writeMessage(_ message: String, to: OutputType = .standard) {
        switch to {
        case .standard:
            print("\(message)")
        case .error:
            fputs("Error: \(message)\n", stderr)
        }
    }
    
    func getInput() -> [String] {
        let keyboard = FileHandle.standardInput
        let inputData = keyboard.availableData
        let strData = String(data: inputData, encoding: String.Encoding.utf8)!
        return strData.trimmingCharacters(in: CharacterSet.newlines).split(separator: " ").map{String($0)}
    }
    
    func printUsage() {
        let usage = """
        SET <key> <value> // store the value for key
        GET <key>         // return the current value for key
        DELETE <key>      // remove the entry for key
        COUNT <value>     // return the number of keys that have the given value
        BEGIN             // start a new transaction
        COMMIT            // complete the current transaction
        ROLLBACK          // revert to state prior to BEGIN call
        QUIT              // terminates CLI

        """
        print(usage)
    }
}
