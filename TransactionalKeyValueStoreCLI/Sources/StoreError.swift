// 

import Foundation

enum StoreError: Error {
    case nothingToCommit
}
