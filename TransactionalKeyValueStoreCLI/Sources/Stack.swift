
import Foundation

class Node<T> {
    var nodeValue:T
    var next:Node?
    init(value:T) {
        self.nodeValue = value
    }
}

class Stack<T> {
    let serialQueue = DispatchQueue(label: "serial.queue")
    var head:Node<T>?
    func push(content:T){
        serialQueue.sync {
            let node = Node<T>(value: content)
            if let headNode = head {
                node.next = headNode
                head = node
            }
            else {
                head = node
            }
        }
    }
    func pop() -> Node<T>? {
        serialQueue.sync {
            if let headNode = head {
                let node = headNode
                head = headNode.next
                node.next = nil
                return node
            }
            return nil
        }
    }
    func peek () -> Node<T>? {
        return head
    }
}
