
public class TransactionalStore {
    private let transactionsStack = Stack<Transaction>()
    private var globalStore = [String: String]()
    
    public func set(_ key: String, _ value: String) {
        if let topTransaction = transactionsStack.peek()?.nodeValue {
            topTransaction.store[key] = value
        } else {
            globalStore[key] = value
        }
    }
    
    public func get(_ key: String) -> String? {
        if let topTransaction = transactionsStack.peek()?.nodeValue,
           let value = topTransaction.store[key] {
            return value
        } else if let value = globalStore[key] {
            return value
        } else {
            return nil
        }
    }
    
    public func delete(_ key: String) {
        if let topTransaction = transactionsStack.peek()?.nodeValue {
            topTransaction.store[key] = nil
        } else {
            globalStore[key] = nil
        }
    }
    
    public func count(_ value: String) -> Int {
        let globalStoreCount = globalStore.values.filter{$0 == value}.count
        let topTransactionStoreCount = transactionsStack.peek()?.nodeValue.store.values.filter{$0 == value}.count ?? 0
        return globalStoreCount + topTransactionStoreCount
    }

    public func begin() {
        let newTransaction = Transaction()
        transactionsStack.push(content: newTransaction)
    }
    
    public func commit() throws {
        if let activeTransaction = transactionsStack.peek()?.nodeValue {
            for (key, value) in activeTransaction.store {
                globalStore[key] = value
                if let nextTransaction = activeTransaction.next {
                    nextTransaction.store[key] = value
                }
            }
        } else {
            throw StoreError.nothingToCommit
        }
    }
    
    public func rollBack() -> Transaction? {
        return transactionsStack.pop()?.nodeValue
    }
}
