// 

import XCTest
@testable import TransactionalKeyValueStoreCLI

class TransactionalKeyValueStoreTest: XCTestCase {

    var sut: TransactionalStore!
    
    override func setUp() {
        super.setUp()
        sut = TransactionalStore()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func testSetAndGet() {
        let value = "123"
        sut.set("foo", "123")
        let foo = sut.get("foo")
        XCTAssertEqual(foo, value)
    }
    
    func testDeleteValue() {
        sut.set("foo", "123")
        sut.delete("foo")
        let value = sut.get("foo")
        XCTAssertNil(value)
    }
    
    func testNumberOfOccurrences() {
        sut.set("foo", "123")
        sut.set("bar", "456")
        sut.set("baz", "123")
        var count = sut.count("123")
        XCTAssertEqual(count, 2)
        sut.begin()
        sut.set("bak", "123")
        count = sut.count("123")
        XCTAssertEqual(count, 3)
    }
    
    func testCommitTransaction() {
        sut.begin()
        sut.set("foo", "456")
        try! sut.commit()
        _ = sut.rollBack()
        let value = sut.get("foo")
        XCTAssertEqual(value, "456")
    }
    
    func testRollBackTransaction() {
        sut.set("foo", "123")
        sut.begin()
        sut.set("foo", "456")
        var foo = sut.get("foo")
        XCTAssertEqual("456", foo)
        _ = sut.rollBack()
        foo = sut.get("foo")
        XCTAssertEqual(foo, "123")
        XCTAssertThrowsError(try sut.commit())
    }
    
    func testNestedTransactions() {
        sut.set("foo", "123")
        sut.begin()
        sut.set("foo", "456")
        sut.begin()
        sut.set("foo", "789")
        var foo = sut.get("foo")
        XCTAssertEqual(foo, "789")
        _ = sut.rollBack()
        foo = sut.get("foo")
        XCTAssertEqual(foo, "456")
        _ = sut.rollBack()
        foo = sut.get("foo")
        XCTAssertEqual(foo, "123")
    }
}
